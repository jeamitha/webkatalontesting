<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>72b3cfef-3abe-416a-9bae-8362376abbe4</testSuiteGuid>
   <testCaseLink>
      <guid>b239e09b-3f4c-4fa2-b0dc-2c1d0523d0da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC_01_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b7dfa53-9b0d-4e27-a1ad-6e45779a4940</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC_02_WrongUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbc1b740-6bda-492b-a1cf-9db0d43d915e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC_03_WrongPass</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb7a0bc6-7cc6-438a-ad2e-320bbdc2b441</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC_04_WrongUserPass</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
